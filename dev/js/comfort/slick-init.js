$('.main-slider').slick({
    dots: true,
    infinite: true,
    speed: 500,
    autoplay: true,
    lazyLoad: 'ondemand'
})


$('.slider-2').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    dots: false,
    lazyLoad: 'ondemand',
    appendArrows: $(".buttons__slider")
})

$('.slick-dots li button').on('click', function(e){
    e.stopPropagation();
});