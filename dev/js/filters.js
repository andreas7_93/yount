$(document).ready(function(){
    let cardsWrapper = $('.cards__wrapper'),
        viewModeValue = $('input[name="view-mode"]'),
        cardsHeader = $('.cards__header__row'),
        typeGroup = $('#type-group');

        if(cardsWrapper && viewModeValue){
            viewModeValue.on('click', function(e){
                if(this.value === 'view-mode-tile' && cardsWrapper.hasClass("cards__wrapper-row")){
                    cardsWrapper.removeClass('cards__wrapper-row');
                    $('.cards__header-show').removeClass('cards__header-show');
                }

                if(this.value === 'view-mode-simple' && !cardsWrapper.hasClass("cards__wrapper-row")){
                    cardsWrapper.addClass('cards__wrapper-row');
                    if($('#type-group').prop('checked')){
                        $('.cards__header-show').removeClass('cards__header-show');
                        $('.cards__header__row-group').addClass('cards__header-show')
                    }
                    else
                        document.querySelectorAll('.cards__header__row')[0].classList.add('cards__header-show');
                };
            });
        }
        if(typeGroup){
            typeGroup.on('click', function(e){
                    let viewMode = $('input[name="view-mode"]:checked').attr('value');
                    $('.cards__header-show').removeClass('cards__header-show');

                    if (e.target.checked && viewMode === "view-mode-simple")
                        $('.cards__header__row-group').addClass('cards__header-show');
                    if(!e.target.checked && viewMode === "view-mode-simple")
                        document.querySelectorAll('.cards__header__row')[0].classList.add('cards__header-show');
            });
        }

        let closeFilters = $('.close__filters__form'),
            filtersForm = $('.filters__form'),
            showFilter = $('.show-filter');
        if(showFilter){
            showFilter.on('click', function(){
                filtersForm.toggleClass('filters__form-active');
            });
        }

        closeFilters.on('click', function(){
            filtersForm.toggleClass('filters__form-active');

        })
});