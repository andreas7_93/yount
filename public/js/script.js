$(document).ready(function(){
    const element = document.querySelector('.js-choice');
    const choices = new Choices(element, {
        searchEnabled: false,
    });
    let tooltipElem;

    function tooltipShow(event){
        let target = event.target;

        //если есть подсказка
        let tooltipHtml = target.dataset.tooltip
        if(!tooltipHtml) return;

        //создание элемента подсказки
        tooltipElem = document.createElement('div');
        tooltipElem.className = 'tooltip';
        tooltipElem.innerHTML = tooltipHtml;
        document.body.append(tooltipElem);

        //позиционирование сверху
        let coords = target.getBoundingClientRect();

        let left = coords.left + (target.offsetWidth - tooltipElem.offsetWidth) / 2;
        if(left < 0) left = 0; //не заезжать за левый край окна

        let top = coords.top - tooltipElem.offsetHeight - 5;
        if(top < 0) top = coords.top + target.offsetHeight + 5; //если подсказка не помещается сверху, то отображать ее снизу

        tooltipElem.style.left = left + 'px';
        tooltipElem.style.top = top + 'px';
    }

    function tooltipHide(event){
        if(tooltipElem){
            tooltipElem.remove();
            tooltipElem = null;
        }
    }

    $( "[data-tooltip]" ).mouseover(() => tooltipShow(event));
    $( "[data-tooltip]" ).mouseout(() => tooltipHide(event));
    $( "[data-tooltip]" ).click(() => {
        tooltipShow(event);
        let timTooltip = setTimeout(() => {
            if($('.tooltip')){
                $('.tooltip').remove();
            }
        }, 2000);

    });



    let allParametnsBtn = $('.all-parametrs__wrapper input[type="checkbox"]'),
        filtersFormRow = $('.filters__form__content .filters__form__row')[0];
    if(allParametnsBtn) {
        allParametnsBtn.on('change', function () {
            if (this.checked)
                filtersFormRow.classList.remove('filters__form__row-all');
            else
                filtersFormRow.classList.add('filters__form__row-all');
        })
    }





});