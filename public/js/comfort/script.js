$(document).ready(function(){
    let tabsItem = $(".tabs__wrapper .tabs__item"),
        tabsContent = $(".tabs__content");
    tabsItem.on('click', function(){
        $(".tabs__wrapper .tabs__item-active").removeClass("tabs__item-active")
        $(this).addClass("tabs__item-active")
        $(".tabs__content-active").removeClass('tabs__content-active')
        let dataKey = $(this).attr("data-key")
        $(".tabs__content[data-content="+ dataKey +"]").addClass('tabs__content-active');
    })

    let finishingTabsItem = $(".finishing__tabs .finishing__tab__item");
    finishingTabsItem.on('click', function(){
        $(".finishing__tab__item.active").removeClass("active")
        $(this).addClass("active")
        $(".finishing__tabs__content.active").removeClass('active')
        let dataKey = $(this).attr("data-key")
        $(".finishing__tabs__content[data-content="+ dataKey +"]").addClass('active');
    })

    let comfortTypeTab = $(" .finishing__tabs__content[data-content='comfort'] .comfort-type__tabs .comfort-type__tab");
    comfortTypeTab.on('click', function(){
        $(".finishing__tabs__content[data-content='comfort'] .comfort-type__tab.active").removeClass("active")
        $(this).addClass("active")
        $(".finishing__tabs__content[data-content='comfort'] .comfort-type__content.active").removeClass('active')
        let dataKey = $(this).attr("data-key");
        $(".comfort-type__content[data-content="+ dataKey +"]").addClass('active')
        $(".finishing__tabs__content .finishing__items").removeClass('vanil coffe')
        $(".finishing__tabs__content .finishing__items").addClass(dataKey)
    })
})
$('.slider__wrapper').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    dots: false,
    lazyLoad: 'ondemand',
})

$('.finishing-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    dots: false,
    lazyLoad: 'ondemand',
})

$('.slider__wrapper-dots').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    dots: true,
    lazyLoad: 'ondemand',
    appendDots: $(".dots__wrapper")
})